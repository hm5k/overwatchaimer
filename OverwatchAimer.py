__author__ = 'Tescrin'

from PIL import Image
from PIL import ImageGrab
import wx

# get all display's resolution
app = wx.App(False)
displays = (wx.Display(i) for i in range(wx.Display.GetCount()))
sizes = [display.GetGeometry().GetSize() for display in displays]

# get image size
# im = Image.open("test_image_1.png")
im = Image.open("test_image_2.jpg")
# im = Image.open("test_image_3.jpg")
# im = Image.open("test_image_4.jpg")
size = im.size

# build box around just a portion of the middle of the screen
BOX_DIVIDER = 12

width = size[0]
height = size[1]
half_width = width/2
half_box_size = width/BOX_DIVIDER
box_size = 2 * half_box_size
half_height = height/2

box = (half_width - half_box_size, half_height - half_box_size,
       half_width + half_box_size, half_height + half_box_size)

cropped_im = im.crop(box)
# cropped_im.show()  # works appropriately

# get list of pixels, getdata is COLUMN_MAJOR ordered
list_of_pixels = list(cropped_im.getdata())

pixels_we_care_about = []
num_red_pixels = 0
added_indices = 0
for index, pixel in enumerate(list_of_pixels):
    if (pixel[0] > 180) \
    and (pixel[1] < 75) \
    and (pixel[1] > 20) \
    and (pixel[2] > 20) \
    and (pixel[2] < 45):
        pixels_we_care_about.append(pixel)
        added_indices += index
        num_red_pixels += 1
    else:
        pixels_we_care_about.append([0, 0, 0, 255])

# pixel_to_change = (box_size * box_size) - int(added_indices / num_red_pixels)
# pixels_we_care_about[pixel_to_change] = [255, 255, 255, 255]

new_im = Image.new(cropped_im.mode, cropped_im.size)
new_im.putdata([tuple(pixel) for pixel in pixels_we_care_about])
new_im.show()

'''

# get screen size
# Note: of second monitor. I run games on the second monitor
screenwidth = sizes[1][0]
screenheight = sizes[1][1]

# build box around just a portion of the middle of the screen
BOX_DIVIDER = 12

half_width = screenwidth/2
twelvth_width = screenwidth/BOX_DIVIDER
half_height = screenheight/2
twelvth_height = screenheight/BOX_DIVIDER

# send that box into the image grab
im = ImageGrab.grab(box)

# iterate over the image grab and average together coordinates for each of x
# and y to find the rough intended destination

# determine relative difference between averaged position and center
# send some percentage of that difference to the mouse controller
'''